package com.cognizant.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="role")
public class Role {
	
 @Id
 @Column(name="role")
 private String role;
 
 @Column(name="role_id")
 private int id;

 public int getId() {
  return id;
 }

 public void setId(int id) {
  this.id = id;
 }

 public String getRole() {
  return role;
 }

 public void setRole(String role) {
  this.role = role;
 }

 
 
}
